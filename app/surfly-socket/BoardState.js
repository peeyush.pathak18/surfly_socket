/**
 * Created by peeyushpathak on 07/07/18.
 */


var mongo = require('mongodb');
var monk = require('monk');
var db = monk('localhost:27017/nodetest1');

function BoardState() {
    //this will be used to store the clients with respect to their GUIDS
    this.client_id_map = {};
    //this will store the board state
    this.board = {};
    //this will store the client details
    this.client_detail_map = {};
    //this will store the guid vs client id mapping for reverse lookup
    this.client_guid_reverse_map = {};
    this.color_available_map = {"red":true, "green":true, "blue":true};

    var self = this;
    this.getClientFor = function(guid){
        if(self.client_id_map.hasOwnProperty(guid)){
            return self.client_id_map[guid];
        }else{
            return undefined;
        }
    };


    this.getBoardState = function () {
      return Object.assign({},self.board);
    };

    this.getNewAvailableColor = function () {
      return "red";
    };

    this.addClientFor = function (client_details, client) {
        self.client_guid_reverse_map[client.id] = client_details.guid;
        client.guid = client_details.guid;
        client.name = client_details.name;
        client.color = self.getNewAvailableColor();
        self.client_id_map[client_details.guid] = client;
        //TODO add color to details
        self.client_detail_map[client.id] = {name:client.name, color:client.color};
    };

    this.getClientGUIDFor = function(client_id){

      if(self.client_guid_reverse_map.hasOwnProperty(client_id)){
          return self.client_guid_reverse_map[client_id];
      }else{
          return undefined;
      }
    };



    this.getUserNames = function () {
        var names = [];
        for(var i in self.client_detail_map){
            names.push(self.client_detail_map[i].name)
        }
      return names;
    };


    /**
     * this is a function to remove client in case of disconnection
     * @param client
     */
    this.removeClient = function (client) {
        delete self.client_id_map[self.getClientGUIDFor(client.id)];
        delete  self.client_guid_reverse_map[client.id];
        delete self.client_detail_map[client.id]
    };

    this.clearBoard = function () {
        return new Promise(function (resolve, revert) {
            self.board = {};
            return resolve();
        });
    };


    /**
     * this function will update the baord state based on new chage array from a different array
     * @param newChange
     * @param client_id
     */
    this.updateBoard = function (newChange, client_id) {
        return new Promise(function (resolve, reject) {
            if(!self.board.hasOwnProperty(client_id)){
                self.board[client_id] = {data:[], color:self.client_detail_map.color}
            }

            self.board[client_id].data.push(newChange);
            resolve();
        });
    };


    this.getAllClients = function () {
       var clients = [];
        for (var id in self.client_id_map){
            clients.push(self.client_id_map[id]);
        }

        return clients;
    };

}



var boardState = new BoardState();
module.exports = boardState;