/**
 * Created by peeyushpathak on 07/07/18.
 */

const board_state = require("./BoardState");
const uuidv1 = require('uuid/v1');

function GameController() {
    var self = this;


    this.updateBoardStateToAll = function () {
        var update_res = {};
        update_res.board_state = board_state.getBoardState();
        update_res.users = board_state.getUserNames();
        var clients = board_state.getAllClients();

        return status = clients.map(function (client, index) {
            client.emit("screen_update", update_res);
        });

    };

    this.registerClient = function (client_details, client) {
        if(!client_details.guid || !board_state.getClientFor(client_details.guid)){
            if(!client_details.guid){
                client_details.guid = uuidv1();
            }
            board_state.addClientFor(client_details, client);
        }

        var registration_res = {id:client_details.guid, name:client_details.name};
        registration_res.board_state = board_state.getBoardState();
        client.emit("register_complete", registration_res);
    };


    this.updateUi = function (data, client) {
        board_state.updateBoard(data, client.id).then(
            function () {
                self.updateBoardStateToAll();
            }
        );
    };

    this.clearBoard = function () {
      board_state.clearBoard().then(
          function (data) {
              self.updateBoardStateToAll();
          }
      );
    };


    this.onDisconnect = function (client, error) {
        //TODO
        var guid = board_state.getClientGUIDFor(client.id);
        client = board_state.getClientFor(guid);
        if(!client){
            return;
        }

        self.updateBoardStateToAll();
        // and move broadcast the result
    };

}


var gameController = new GameController();
module.exports = gameController;