/**
 * Created by peeyushpathak on 02/06/18.
 */
var socketio = require('socket.io');
const screenController = require("./ScreenController");


function Connection() {
    this.io = null;
    var self = this;


    this.connect = function (server) {
        self.io = socketio(server);
        self.io.on('connection', self.onconnect);
        self.registerMiddlewares()
    };

    this.registerMiddlewares = function () {
      self.io.use(function (socket, next) {
          // console.log(socket.request);
          next();
      });

        // self.io.use(function (socket, next) {
        //     socket.request.db = db;
        //     next();
        // });
    };


    this.onconnect = function (conn) {
        // console.log(conn.id);
        self.subscribe_to_timer(conn);
    };

    this.subscribe_to_timer = function (client) {

        client.on("init", function(client_details){
            /**
             * client details have guid
             * client name
             */
            screenController.registerClient(client_details, client);
        });


        client.on("clear_screen", function (data) {

        });

        client.on('screen_update', function (update_data) {
            screenController.updateUi(update_data, client);
        });

        client.on("disconnect", function (event) {
            // console.log("disconnect");
            // console.log(event);
            screenController.onDisconnect(client, event);
        });

        client.on("reconnect", function (connect_event) {

        });

        // client.disconnect()

    };

}

var connection = new Connection();
module.exports = connection;







